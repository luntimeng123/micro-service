package com.example.departmentservice.controller;


import com.example.departmentservice.entity.Department;
import com.example.departmentservice.service.DepartmentServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/departments")
@Slf4j
@RefreshScope
public class DeparmentController {

    @Autowired
    private DepartmentServiceImpl departmentService;

    @PostMapping("/")
    public Department saveDeparment(@RequestBody Department department){
        log.info("inside saveDeparment method in controller.");
        return departmentService.saveDepartment(department);
    }

    @GetMapping("/{id}")
    public Department findDepartmentById(@PathVariable("id") Long departmentId){
        log.info("inside findByDeparmentId method in controller.");
        return departmentService.findDepartmentById(departmentId);
    }

}
